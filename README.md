# Face recognition (emotion)

Použijte model pro detekci obličejů (například Tensorflow Object Detection API) na získání pozice obličejů.
Na těchto obličejích poté rozpoznejte emoce pomocí vámi natrénovaného modelu.

# Spuštění

Pokud chcete spouštět i učení, stáhněte si dataset fer2013 ze stránky [kaggle.com](https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/data)

*  Nainstalujte závislosti ze souboru `requirements.txt`
*  (Spusťte skript `learning.py`, naučené váhy se budou průběžně ukládat do složky `training`)
*  Použijte skripty `model_loader.py`, `facedetector.py` a `predictor.py` pro načtení uloženého modelu, získání obličeje z obrázku a predikci emoce na obrázku. Ukázkové použití je v jupyter notebooku `main.ipynb`