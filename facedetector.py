import numpy as np
import cv2

CLASSIFIER = "haarcascade_frontalface_default.xml"

def extract_faces(image):
    """
    Extract faces from image
    
    Parameters:
        image_path: Path of the image
        
    Returns:
        Coordinates of detected faces and faces converted to format required by out model
    """
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    coords = _detect(image)
    
    faces = _normalize_faces(image, coords)
    faces = np.asarray(faces, "float32")
    faces /= 255
    if len(faces) > 0:
        faces = faces.reshape(faces.shape[0], faces.shape[1], faces.shape[2], 1)
    
    return coords, faces

def extract_faces_path(image_path):
    return extract_faces(_load_image(image_path))

def _load_image(image_path):
    return cv2.imread(image_path)

def _detect(image):    
    classifier = cv2.CascadeClassifier(CLASSIFIER)
    faces_coord = classifier.detectMultiScale(
        image,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    return faces_coord

def _cut_faces(image, faces_coord):    
    faces = []

    for (x, y, w, h) in faces_coord:
        w_rm = int(0.3 * w / 2)
        faces.append(image[y: y + h, x + w_rm: x + w - w_rm])

    return faces

def _resize(images, size=(48, 48)):
    images_norm = []
    
    for image in images:
        # image_norm = cv2.resize(image, size)
        # cv2.normalize(image_norm, image_norm, alpha=0, beta=1, norm_type=cv2.NORM_L2, dtype=cv2.CV_32F)
        if image.shape < size:
            image_norm = cv2.resize(image, size, interpolation=cv2.INTER_AREA)
        else:
            image_norm = cv2.resize(image, size, interpolation=cv2.INTER_CUBIC)
        images_norm.append(image_norm)
        
    return images_norm

def _normalize_faces(image, faces_coord):
    faces = _cut_faces(image, faces_coord)
    faces = _resize(faces)

    return faces
