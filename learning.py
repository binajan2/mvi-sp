import os
import numpy as np
import csv
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization
from keras.losses import categorical_crossentropy
from keras.optimizers import Adam
from keras.regularizers import l2

IMG_SIZE = 48
DATA = "data/fer2013.csv"
TRAIN_SIZE = 28709
TEST_SIZE = 3589

NUM_FEATURES = 64
BATCH_SIZE = 64
EPOCHS = 100

def pixels_to_image(pixels):
    array = np.array(pixels.split(" "), "float32")
    array /= 255
    array = array.reshape(IMG_SIZE, IMG_SIZE)
    return array

def load_data():
    file = open(DATA, mode="r")
    reader = csv.reader(file, delimiter=",")
    next(reader, None)
    data = np.array(list(reader))

    labels = data[:, 0].astype('int32')
    images = np.zeros((labels.shape[0], IMG_SIZE, IMG_SIZE), dtype=np.float64)

    for i, pixels in enumerate(data[:, 1]):
        image = pixels_to_image(pixels)
        images[i, :, :] = image

    return (labels, images)

def maybe_turn_to_one_hot(labels, num_labels):
    if len(labels.shape) == 1:
        one_hot = np.zeros((labels.shape[0], num_labels))
        one_hot[np.arange(len(labels)), labels] = 1
        return one_hot
    else:
        return labels

# LOAD DATA =====================================

print("Loading data from", DATA)

labels, images = load_data()

print("Loaded", len(images), "images")

images = images.reshape(images.shape[0], images.shape[1], images.shape[2], 1)

train_data, test_data, valid_data = np.split(images, [TRAIN_SIZE, TRAIN_SIZE+TEST_SIZE])
train_labels, test_labels, valid_labels = np.split(labels, [TRAIN_SIZE, TRAIN_SIZE+TEST_SIZE])

print("Splitted into", len(train_data), "train images,", len(test_data), "test images and", len(valid_data), "validation images.")

mean = np.mean(train_data, axis = 0)
std = np.std(train_data, axis = 0)

train_data = (train_data - mean) / std
test_data = (test_data - mean) / std
valid_data = (valid_data - mean) / std

train_labels = maybe_turn_to_one_hot(train_labels, 7)
test_labels = maybe_turn_to_one_hot(test_labels, 7)
valid_labels = maybe_turn_to_one_hot(valid_labels, 7)

# START LEARNING ================================

model = Sequential([
    Conv2D(NUM_FEATURES, kernel_size=(3, 3), activation='relu', input_shape=(IMG_SIZE, IMG_SIZE, 1), data_format='channels_last', kernel_regularizer=l2(0.01)),
    Conv2D(NUM_FEATURES, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Dropout(0.5),

    Conv2D(2*NUM_FEATURES, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    Conv2D(2*NUM_FEATURES, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Dropout(0.5),

    Conv2D(2*2*NUM_FEATURES, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    Conv2D(2*2*NUM_FEATURES, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Dropout(0.5),

    Conv2D(2*2*2*NUM_FEATURES, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    Conv2D(2*2*2*NUM_FEATURES, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Dropout(0.5),

    Flatten(),
    
    Dense(2*2*2*NUM_FEATURES, activation='relu'),
    Dropout(0.4),
    Dense(2*2*NUM_FEATURES, activation='relu'),
    Dropout(0.4),
    Dense(2*NUM_FEATURES, activation='relu'),
    Dropout(0.5),

    Dense(7, activation='softmax')
])
    
model.compile(
    loss=categorical_crossentropy,
    optimizer=Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-7),
    metrics=['accuracy'])

model.save('model.h5')

checkpoint_path = "training/cp-{epoch:04d}.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path, 
    verbose=1, 
    save_weights_only=True,
    period=1
)

model.save_weights(checkpoint_path.format(epoch=0))

model.fit(
    np.array(train_data), np.array(train_labels),
    batch_size=BATCH_SIZE,
    epochs=EPOCHS,
    verbose=1,
    validation_data=(np.array(valid_data), np.array(valid_labels)),
    shuffle=True,
    callbacks=[cp_callback]
)
