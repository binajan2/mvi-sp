import numpy as np
import csv
import tensorflow as tf
from keras.applications.vgg16 import VGG16
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.losses import categorical_crossentropy
from keras.optimizers import Adam

IMG_SIZE = 48
DATA = "data/fer2013.csv"
TRAIN_SIZE = 28709
TEST_SIZE = 3589

NUM_FEATURES = 64
BATCH_SIZE = 64
EPOCHS = 100

def pixels_to_image(pixels):
    array = np.array(pixels.split(" "), "float32")
    array /= 255
    array = array.reshape(IMG_SIZE, IMG_SIZE)
    return array

def load_data():
    file = open(DATA, mode="r")
    reader = csv.reader(file, delimiter=",")
    next(reader, None)
    data = np.array(list(reader))

    labels = data[:, 0].astype('int32')
    images = np.zeros((labels.shape[0], IMG_SIZE, IMG_SIZE), dtype=np.float64)

    for i, pixels in enumerate(data[:, 1]):
        image = pixels_to_image(pixels)
        images[i, :, :] = image

    return (labels, images)

def maybe_turn_to_one_hot(labels, num_labels):
    if len(labels.shape) == 1:
        one_hot = np.zeros((labels.shape[0], num_labels))
        one_hot[np.arange(len(labels)), labels] = 1
        return one_hot
    else:
        return labels

# LOAD DATA =====================================

print("Loading data from", DATA)

labels, images = load_data()

print("Loaded", len(images), "images")

# images = images.reshape(images.shape[0], images.shape[1], images.shape[2], 1)

train_data, test_data, valid_data = np.split(images, [TRAIN_SIZE, TRAIN_SIZE+TEST_SIZE])
train_labels, test_labels, valid_labels = np.split(labels, [TRAIN_SIZE, TRAIN_SIZE+TEST_SIZE])

print("Splitted into", len(train_data), "train images,", len(test_data), "test images and", len(valid_data), "validation images.")

mean = np.mean(train_data, axis = 0)
std = np.std(train_data, axis = 0)

train_data = (train_data - mean) / std
test_data = (test_data - mean) / std
valid_data = (valid_data - mean) / std

train_labels = maybe_turn_to_one_hot(train_labels, 7)
test_labels = maybe_turn_to_one_hot(test_labels, 7)
valid_labels = maybe_turn_to_one_hot(valid_labels, 7)

# START LEARNING ================================

def duplicate_input_layer(inArray):
    outArray = np.empty([len(inArray), 48, 48, 3])
    for index, item in enumerate(outArray):
        item[:, :, 0] = inArray[index]
        item[:, :, 1] = inArray[index]
        item[:, :, 2] = inArray[index]
    return outArray

def get_vgg16_output(vgg16, inArray):
    dupArray = duplicate_input_layer(inArray)
    return vgg16.predict(dupArray, verbose = True)

vgg16 = VGG16(include_top = False, input_shape = (48, 48, 3), pooling = 'avg', weights = 'imagenet')

# get vgg16 outputs
x_train = get_vgg16_output(vgg16, train_data)
x_test = get_vgg16_output(vgg16, test_data)
x_valid = get_vgg16_output(vgg16, valid_data)

model = Sequential([
    Dense(2*2*2*NUM_FEATURES, input_shape=(512,), activation='relu'),
    Dropout(0.4),
    Dense(2*2*NUM_FEATURES, activation='relu'),
    Dropout(0.4),
    Dense(2*NUM_FEATURES, activation='relu'),
    Dropout(0.5),
    Dense(7, activation='softmax')
])
    
model.compile(
    loss=categorical_crossentropy,
    optimizer=Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-7),
    metrics=['accuracy'])

model.fit(
    x_train, train_labels,
    batch_size=BATCH_SIZE,
    epochs=EPOCHS,
    verbose=1,
    validation_data=(x_valid, valid_labels),
    shuffle=True
)
