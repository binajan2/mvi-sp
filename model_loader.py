import keras
import tensorflow as tf

DEFAULT_MODEL_PATH = "model.h5"
DEFAULT_WEIGHTS_PATH = "training/cp-0040.ckpt"

def load_model(
    modelPath = DEFAULT_MODEL_PATH,
    weightsPath = DEFAULT_WEIGHTS_PATH
    ):
    """Loads keras model from provided model file and weights from provided weights file"""
    
    model = tf.keras.models.load_model(modelPath)
    model.load_weights(weightsPath)
    return model
    