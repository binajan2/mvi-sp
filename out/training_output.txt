Train on 28709 samples, validate on 3589 samples
Epoch 1/100
28709/28709 [==============================] - 1164s 41ms/step - loss: 1.9982 - accuracy: 0.2169 - val_loss: 1.8322 - val_accuracy: 0.2449

Epoch 00001: saving model to training/cp-0001.ckpt
Epoch 2/100
28709/28709 [==============================] - 1199s 42ms/step - loss: 1.8320 - accuracy: 0.2473 - val_loss: 1.8099 - val_accuracy: 0.2471

Epoch 00002: saving model to training/cp-0002.ckpt
Epoch 3/100
28709/28709 [==============================] - 1093s 38ms/step - loss: 1.7486 - accuracy: 0.2879 - val_loss: 1.6545 - val_accuracy: 0.3204

Epoch 00003: saving model to training/cp-0003.ckpt
Epoch 4/100
28709/28709 [==============================] - 1081s 38ms/step - loss: 1.6407 - accuracy: 0.3478 - val_loss: 1.5438 - val_accuracy: 0.4062

Epoch 00004: saving model to training/cp-0004.ckpt
Epoch 5/100
28709/28709 [==============================] - 1196s 42ms/step - loss: 1.5255 - accuracy: 0.4038 - val_loss: 1.4285 - val_accuracy: 0.4444

Epoch 00005: saving model to training/cp-0005.ckpt
Epoch 6/100
28709/28709 [==============================] - 1256s 44ms/step - loss: 1.4641 - accuracy: 0.4267 - val_loss: 1.3440 - val_accuracy: 0.4790

Epoch 00006: saving model to training/cp-0006.ckpt
Epoch 7/100
28709/28709 [==============================] - 1196s 42ms/step - loss: 1.4149 - accuracy: 0.4541 - val_loss: 1.3424 - val_accuracy: 0.4893

Epoch 00007: saving model to training/cp-0007.ckpt
Epoch 8/100
28709/28709 [==============================] - 1100s 38ms/step - loss: 1.3797 - accuracy: 0.4739 - val_loss: 1.2962 - val_accuracy: 0.5060

Epoch 00008: saving model to training/cp-0008.ckpt
Epoch 9/100
28709/28709 [==============================] - 1086s 38ms/step - loss: 1.3436 - accuracy: 0.4911 - val_loss: 1.2690 - val_accuracy: 0.5132

Epoch 00009: saving model to training/cp-0009.ckpt
Epoch 10/100
28709/28709 [==============================] - 1085s 38ms/step - loss: 1.3127 - accuracy: 0.5061 - val_loss: 1.2290 - val_accuracy: 0.5266

Epoch 00010: saving model to training/cp-0010.ckpt
Epoch 11/100
28709/28709 [==============================] - 1116s 39ms/step - loss: 1.2863 - accuracy: 0.5183 - val_loss: 1.2061 - val_accuracy: 0.5397

Epoch 00011: saving model to training/cp-0011.ckpt
Epoch 12/100
28709/28709 [==============================] - 1126s 39ms/step - loss: 1.2549 - accuracy: 0.5275 - val_loss: 1.2053 - val_accuracy: 0.5469

Epoch 00012: saving model to training/cp-0012.ckpt
Epoch 13/100
28709/28709 [==============================] - 1078s 38ms/step - loss: 1.2372 - accuracy: 0.5365 - val_loss: 1.1698 - val_accuracy: 0.5578

Epoch 00013: saving model to training/cp-0013.ckpt
Epoch 14/100
28709/28709 [==============================] - 1064s 37ms/step - loss: 1.2095 - accuracy: 0.5513 - val_loss: 1.1781 - val_accuracy: 0.5481

Epoch 00014: saving model to training/cp-0014.ckpt
Epoch 15/100
28709/28709 [==============================] - 1058s 37ms/step - loss: 1.1940 - accuracy: 0.5576 - val_loss: 1.1715 - val_accuracy: 0.5642

Epoch 00015: saving model to training/cp-0015.ckpt
Epoch 16/100
28709/28709 [==============================] - 1058s 37ms/step - loss: 1.1719 - accuracy: 0.5694 - val_loss: 1.1555 - val_accuracy: 0.5715

Epoch 00016: saving model to training/cp-0016.ckpt
Epoch 17/100
28709/28709 [==============================] - 1056s 37ms/step - loss: 1.1528 - accuracy: 0.5784 - val_loss: 1.1239 - val_accuracy: 0.5832

Epoch 00017: saving model to training/cp-0017.ckpt
Epoch 18/100
28709/28709 [==============================] - 1058s 37ms/step - loss: 1.1295 - accuracy: 0.5850 - val_loss: 1.1012 - val_accuracy: 0.5854

Epoch 00018: saving model to training/cp-0018.ckpt
Epoch 19/100
28709/28709 [==============================] - 1083s 38ms/step - loss: 1.0985 - accuracy: 0.5988 - val_loss: 1.0961 - val_accuracy: 0.6027

Epoch 00019: saving model to training/cp-0019.ckpt
Epoch 20/100
28709/28709 [==============================] - 1122s 39ms/step - loss: 1.0807 - accuracy: 0.6086 - val_loss: 1.0763 - val_accuracy: 0.5968

Epoch 00020: saving model to training/cp-0020.ckpt
Epoch 21/100
28709/28709 [==============================] - 1100s 38ms/step - loss: 1.0608 - accuracy: 0.6140 - val_loss: 1.0522 - val_accuracy: 0.6094

Epoch 00021: saving model to training/cp-0021.ckpt
Epoch 22/100
28709/28709 [==============================] - 1209s 42ms/step - loss: 1.0441 - accuracy: 0.6242 - val_loss: 1.0651 - val_accuracy: 0.6052

Epoch 00022: saving model to training/cp-0022.ckpt
Epoch 23/100
28709/28709 [==============================] - 1152s 40ms/step - loss: 1.0184 - accuracy: 0.6325 - val_loss: 1.0292 - val_accuracy: 0.6199

Epoch 00023: saving model to training/cp-0023.ckpt
Epoch 24/100
28709/28709 [==============================] - 1067s 37ms/step - loss: 0.9985 - accuracy: 0.6403 - val_loss: 1.0353 - val_accuracy: 0.6127

Epoch 00024: saving model to training/cp-0024.ckpt
Epoch 25/100
28709/28709 [==============================] - 1056s 37ms/step - loss: 0.9759 - accuracy: 0.6489 - val_loss: 1.0359 - val_accuracy: 0.6166

Epoch 00025: saving model to training/cp-0025.ckpt
Epoch 26/100
28709/28709 [==============================] - 1137s 40ms/step - loss: 0.9545 - accuracy: 0.6558 - val_loss: 1.0380 - val_accuracy: 0.6197

Epoch 00026: saving model to training/cp-0026.ckpt
Epoch 27/100
28709/28709 [==============================] - 1306s 45ms/step - loss: 0.9369 - accuracy: 0.6659 - val_loss: 1.0281 - val_accuracy: 0.6208

Epoch 00027: saving model to training/cp-0027.ckpt
Epoch 28/100
28709/28709 [==============================] - 1199s 42ms/step - loss: 0.9230 - accuracy: 0.6688 - val_loss: 1.0153 - val_accuracy: 0.6294

Epoch 00028: saving model to training/cp-0028.ckpt
Epoch 29/100
28709/28709 [==============================] - 1138s 40ms/step - loss: 0.9136 - accuracy: 0.6740 - val_loss: 1.0017 - val_accuracy: 0.6336

Epoch 00029: saving model to training/cp-0029.ckpt
Epoch 30/100
28709/28709 [==============================] - 1192s 42ms/step - loss: 0.8904 - accuracy: 0.6827 - val_loss: 1.0385 - val_accuracy: 0.6322

Epoch 00030: saving model to training/cp-0030.ckpt
Epoch 31/100
28709/28709 [==============================] - 1197s 42ms/step - loss: 0.8710 - accuracy: 0.6867 - val_loss: 1.0190 - val_accuracy: 0.6305

Epoch 00031: saving model to training/cp-0031.ckpt
Epoch 32/100
28709/28709 [==============================] - 1390s 48ms/step - loss: 0.8538 - accuracy: 0.6944 - val_loss: 1.0323 - val_accuracy: 0.6266

Epoch 00032: saving model to training/cp-0032.ckpt
Epoch 33/100
28709/28709 [==============================] - 1285s 45ms/step - loss: 0.8360 - accuracy: 0.7033 - val_loss: 1.0160 - val_accuracy: 0.6358

Epoch 00033: saving model to training/cp-0033.ckpt
Epoch 34/100
28709/28709 [==============================] - 1060s 37ms/step - loss: 0.8229 - accuracy: 0.7097 - val_loss: 1.0508 - val_accuracy: 0.6353

Epoch 00034: saving model to training/cp-0034.ckpt
Epoch 35/100
28709/28709 [==============================] - 1048s 37ms/step - loss: 0.8136 - accuracy: 0.7111 - val_loss: 1.0358 - val_accuracy: 0.6339

Epoch 00035: saving model to training/cp-0035.ckpt
Epoch 36/100
28709/28709 [==============================] - 1043s 36ms/step - loss: 0.7800 - accuracy: 0.7256 - val_loss: 1.0658 - val_accuracy: 0.6278

Epoch 00036: saving model to training/cp-0036.ckpt
Epoch 37/100
28709/28709 [==============================] - 1044s 36ms/step - loss: 0.7724 - accuracy: 0.7246 - val_loss: 1.0167 - val_accuracy: 0.6400

Epoch 00037: saving model to training/cp-0037.ckpt
Epoch 38/100
28709/28709 [==============================] - 1044s 36ms/step - loss: 0.7634 - accuracy: 0.7329 - val_loss: 1.0291 - val_accuracy: 0.6459

Epoch 00038: saving model to training/cp-0038.ckpt
Epoch 39/100
28709/28709 [==============================] - 1042s 36ms/step - loss: 0.7557 - accuracy: 0.7372 - val_loss: 1.0444 - val_accuracy: 0.6356

Epoch 00039: saving model to training/cp-0039.ckpt
Epoch 40/100
28709/28709 [==============================] - 1079s 38ms/step - loss: 0.7316 - accuracy: 0.7461 - val_loss: 1.0225 - val_accuracy: 0.6439

Epoch 00040: saving model to training/cp-0040.ckpt
Epoch 41/100
28709/28709 [==============================] - 1076s 37ms/step - loss: 0.7138 - accuracy: 0.7515 - val_loss: 1.0719 - val_accuracy: 0.6428

Epoch 00041: saving model to training/cp-0041.ckpt
Epoch 42/100
28709/28709 [==============================] - 1207s 42ms/step - loss: 0.6994 - accuracy: 0.7571 - val_loss: 1.0400 - val_accuracy: 0.6442

Epoch 00042: saving model to training/cp-0042.ckpt
Epoch 43/100
28709/28709 [==============================] - 1198s 42ms/step - loss: 0.6913 - accuracy: 0.7610 - val_loss: 1.0593 - val_accuracy: 0.6565

Epoch 00043: saving model to training/cp-0043.ckpt
Epoch 44/100
28709/28709 [==============================] - 1200s 42ms/step - loss: 0.6709 - accuracy: 0.7663 - val_loss: 1.0774 - val_accuracy: 0.6512

Epoch 00044: saving model to training/cp-0044.ckpt
Epoch 45/100
28709/28709 [==============================] - 1176s 41ms/step - loss: 0.6720 - accuracy: 0.7694 - val_loss: 1.0550 - val_accuracy: 0.6609

Epoch 00045: saving model to training/cp-0045.ckpt
Epoch 46/100
28709/28709 [==============================] - 1157s 40ms/step - loss: 0.6425 - accuracy: 0.7783 - val_loss: 1.0548 - val_accuracy: 0.6556

Epoch 00046: saving model to training/cp-0046.ckpt
Epoch 47/100
28709/28709 [==============================] - 1257s 44ms/step - loss: 0.6430 - accuracy: 0.7801 - val_loss: 1.0484 - val_accuracy: 0.6537

Epoch 00047: saving model to training/cp-0047.ckpt
Epoch 48/100
28709/28709 [==============================] - 1208s 42ms/step - loss: 0.6344 - accuracy: 0.7861 - val_loss: 1.0612 - val_accuracy: 0.6534

Epoch 00048: saving model to training/cp-0048.ckpt
Epoch 49/100
28709/28709 [==============================] - 1037s 36ms/step - loss: 0.6134 - accuracy: 0.7897 - val_loss: 1.0954 - val_accuracy: 0.6573

Epoch 00049: saving model to training/cp-0049.ckpt
Epoch 50/100
28709/28709 [==============================] - 1031s 36ms/step - loss: 0.6089 - accuracy: 0.7906 - val_loss: 1.0678 - val_accuracy: 0.6486

Epoch 00050: saving model to training/cp-0050.ckpt
Epoch 51/100
28709/28709 [==============================] - 1034s 36ms/step - loss: 0.6061 - accuracy: 0.7945 - val_loss: 1.0827 - val_accuracy: 0.6537

Epoch 00051: saving model to training/cp-0051.ckpt
Epoch 52/100
28709/28709 [==============================] - 1034s 36ms/step - loss: 0.5946 - accuracy: 0.7992 - val_loss: 1.1834 - val_accuracy: 0.6386

Epoch 00052: saving model to training/cp-0052.ckpt
Epoch 53/100
28709/28709 [==============================] - 1037s 36ms/step - loss: 0.5823 - accuracy: 0.8042 - val_loss: 1.1209 - val_accuracy: 0.6439

Epoch 00053: saving model to training/cp-0053.ckpt
Epoch 54/100
28709/28709 [==============================] - 1036s 36ms/step - loss: 0.5597 - accuracy: 0.8120 - val_loss: 1.1780 - val_accuracy: 0.6567

Epoch 00054: saving model to training/cp-0054.ckpt
Epoch 55/100
28709/28709 [==============================] - 1035s 36ms/step - loss: 0.5564 - accuracy: 0.8136 - val_loss: 1.1468 - val_accuracy: 0.6473

Epoch 00055: saving model to training/cp-0055.ckpt
Epoch 56/100
28709/28709 [==============================] - 1035s 36ms/step - loss: 0.5482 - accuracy: 0.8147 - val_loss: 1.1551 - val_accuracy: 0.6514

Epoch 00056: saving model to training/cp-0056.ckpt
Epoch 57/100
28709/28709 [==============================] - 1036s 36ms/step - loss: 0.5423 - accuracy: 0.8200 - val_loss: 1.1315 - val_accuracy: 0.6514

Epoch 00057: saving model to training/cp-0057.ckpt
Epoch 58/100
28709/28709 [==============================] - 1036s 36ms/step - loss: 0.5294 - accuracy: 0.8214 - val_loss: 1.1249 - val_accuracy: 0.6531

Epoch 00058: saving model to training/cp-0058.ckpt
Epoch 59/100
28709/28709 [==============================] - 1034s 36ms/step - loss: 0.5291 - accuracy: 0.8225 - val_loss: 1.1463 - val_accuracy: 0.6562

Epoch 00059: saving model to training/cp-0059.ckpt
Epoch 60/100
28709/28709 [==============================] - 1035s 36ms/step - loss: 0.5153 - accuracy: 0.8288 - val_loss: 1.1851 - val_accuracy: 0.6503

Epoch 00060: saving model to training/cp-0060.ckpt
Epoch 61/100
28709/28709 [==============================] - 1034s 36ms/step - loss: 0.5110 - accuracy: 0.8300 - val_loss: 1.1640 - val_accuracy: 0.6573

Epoch 00061: saving model to training/cp-0061.ckpt
Epoch 62/100
28709/28709 [==============================] - 1034s 36ms/step - loss: 0.4975 - accuracy: 0.8354 - val_loss: 1.1805 - val_accuracy: 0.6520

Epoch 00062: saving model to training/cp-0062.ckpt
Epoch 63/100
28709/28709 [==============================] - 1035s 36ms/step - loss: 0.4891 - accuracy: 0.8368 - val_loss: 1.1922 - val_accuracy: 0.6484

Epoch 00063: saving model to training/cp-0063.ckpt
Epoch 64/100
28709/28709 [==============================] - 1032s 36ms/step - loss: 0.4777 - accuracy: 0.8420 - val_loss: 1.1856 - val_accuracy: 0.6539

Epoch 00064: saving model to training/cp-0064.ckpt
Epoch 65/100
28709/28709 [==============================] - 1032s 36ms/step - loss: 0.4714 - accuracy: 0.8467 - val_loss: 1.1656 - val_accuracy: 0.6525

Epoch 00065: saving model to training/cp-0065.ckpt
Epoch 66/100
28709/28709 [==============================] - 1033s 36ms/step - loss: 0.4682 - accuracy: 0.8447 - val_loss: 1.1870 - val_accuracy: 0.6545

Epoch 00066: saving model to training/cp-0066.ckpt
Epoch 67/100
28709/28709 [==============================] - 1035s 36ms/step - loss: 0.4648 - accuracy: 0.8463 - val_loss: 1.1845 - val_accuracy: 0.6523

Epoch 00067: saving model to training/cp-0067.ckpt
Epoch 68/100
28709/28709 [==============================] - 1033s 36ms/step - loss: 0.4504 - accuracy: 0.8534 - val_loss: 1.2268 - val_accuracy: 0.6506

Epoch 00068: saving model to training/cp-0068.ckpt
Epoch 69/100
28709/28709 [==============================] - 1033s 36ms/step - loss: 0.4435 - accuracy: 0.8529 - val_loss: 1.2433 - val_accuracy: 0.6595

Epoch 00069: saving model to training/cp-0069.ckpt
Epoch 70/100
28709/28709 [==============================] - 1045s 36ms/step - loss: 0.4415 - accuracy: 0.8566 - val_loss: 1.2258 - val_accuracy: 0.6584

Epoch 00070: saving model to training/cp-0070.ckpt
Epoch 71/100
28709/28709 [==============================] - 1036s 36ms/step - loss: 0.4242 - accuracy: 0.8624 - val_loss: 1.1835 - val_accuracy: 0.6475

Epoch 00071: saving model to training/cp-0071.ckpt
Epoch 72/100
28709/28709 [==============================] - 1036s 36ms/step - loss: 0.4192 - accuracy: 0.8617 - val_loss: 1.1927 - val_accuracy: 0.6587

Epoch 00072: saving model to training/cp-0072.ckpt
Epoch 73/100
28709/28709 [==============================] - 1038s 36ms/step - loss: 0.4164 - accuracy: 0.8663 - val_loss: 1.2359 - val_accuracy: 0.6456

Epoch 00073: saving model to training/cp-0073.ckpt
Epoch 74/100
28709/28709 [==============================] - 1038s 36ms/step - loss: 0.4159 - accuracy: 0.8640 - val_loss: 1.2197 - val_accuracy: 0.6509

Epoch 00074: saving model to training/cp-0074.ckpt
Epoch 75/100
28709/28709 [==============================] - 1036s 36ms/step - loss: 0.4066 - accuracy: 0.8668 - val_loss: 1.3338 - val_accuracy: 0.6514

Epoch 00075: saving model to training/cp-0075.ckpt
Epoch 76/100
28709/28709 [==============================] - 1036s 36ms/step - loss: 0.4089 - accuracy: 0.8687 - val_loss: 1.3284 - val_accuracy: 0.6578

Epoch 00076: saving model to training/cp-0076.ckpt
Epoch 77/100
28709/28709 [==============================] - 1038s 36ms/step - loss: 0.3937 - accuracy: 0.8750 - val_loss: 1.2869 - val_accuracy: 0.6631

Epoch 00077: saving model to training/cp-0077.ckpt
Epoch 78/100
28709/28709 [==============================] - 1118s 39ms/step - loss: 0.3883 - accuracy: 0.8727 - val_loss: 1.2438 - val_accuracy: 0.6553

Epoch 00078: saving model to training/cp-0078.ckpt
