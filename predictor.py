LABELS = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']

def predict(model, faces):
    """
    Predict emotions in provided faces using provided model
    
    Parameters: 
        model: Model to use for prediction, prefarably obtained from model_loader.py
        faces: Array of faces to predict, face shape should be (48, 48, 1), pixel value in <0.0, 1.0>
          
    Returns: 
        Dictionary from LABELS to probabilities, sorted by probability descending.
        Probabilities are floats rounded to two decimal places (whole percents), zero probabilities are omitted
    """
    
    predictionsRaw = model.predict(faces)
    predictions = []
    
    for prediction in predictionsRaw:
        d = dict(zip(LABELS, prediction))
        ds = sorted(d.items(), key=lambda item: item[1], reverse=True)
        rounded = {k: round(v, 2) for k, v in ds}
        filtered = dict(filter(lambda elem: elem[1] > 0, rounded.items()))
        predictions.append(filtered)
    
    return predictions
    